import 'player.dart';
import 'dart:io';

class Shop {
  Player open(Player player) {
    bool isExists = false;

    Map info = new Map();
    info['healHp'] = 0;
    info['healStamina'] = 0;

    print('=== SHOP ===');

    while (!isExists) {
      player.printInfo();
      print('1: Buy health potion (+10)');
      print('2: Buy stamina potion (+10)');
      print('3: exists');
      print('Enter number: ');

      int? n = int.parse(stdin.readLineSync()!);

      if (n == 1) {
        print('You bought health potion, player hp +10');
        player.setHp = player.getHp + 10;
      }
      if (n == 2) {
        print('You bought stamina potion, player stamina +10');
        player.setStamina = player.getStamina + 10;
      }

      if (n == 3) isExists = true;
    }

    return player;
  }
}
