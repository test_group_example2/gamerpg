import 'boss.dart';
import 'monster.dart';
import 'player.dart';
import 'dart:io';
import 'dart:math';

class Dungeon {
  List<Monster> monsterLv1 = [
    Monster(
      name: 'Slime',
      hp: 16,
      baseHp: 16,
      minDamage: 1,
      maxDamage: 3,
      minExp: 1,
      maxExp: 3,
    ),
    Monster(
      name: 'Snake',
      hp: 20,
      baseHp: 20,
      minDamage: 2,
      maxDamage: 4,
      minExp: 2,
      maxExp: 4,
    ),
    Monster(
      name: 'Wolf',
      hp: 30,
      baseHp: 30,
      minDamage: 3,
      maxDamage: 7,
      minExp: 4,
      maxExp: 6,
    ),
  ];
  List<Monster> monsterLv2 = [
    Monster(
      name: 'Goblin',
      hp: 40,
      baseHp: 40,
      minDamage: 4,
      maxDamage: 8,
      minExp: 5,
      maxExp: 7,
    ),
    Monster(
      name: 'Orc',
      hp: 50,
      baseHp: 50,
      minDamage: 5,
      maxDamage: 9,
      minExp: 6,
      maxExp: 8,
    ),
    Monster(
      name: 'Troll',
      hp: 60,
      baseHp: 60,
      minDamage: 6,
      maxDamage: 10,
      minExp: 7,
      maxExp: 9,
    )
  ];
  List<Monster> monsterLv3 = [
    Monster(
      name: 'Dragon',
      hp: 100,
      baseHp: 100,
      minDamage: 10,
      maxDamage: 15,
      minExp: 10,
      maxExp: 15,
    ),
    Monster(
      name: 'Golem',
      hp: 80,
      baseHp: 80,
      minDamage: 8,
      maxDamage: 12,
      minExp: 8,
      maxExp: 12,
    ),
    Monster(
      name: 'Giant',
      hp: 70,
      baseHp: 70,
      minDamage: 7,
      maxDamage: 11,
      minExp: 7,
      maxExp: 11,
    )
  ];

  Boss boss1 = Boss(
    name: 'Demon Lord',
    hp: 200,
    baseHp: 200,
    minDamage: 15,
    maxDamage: 20,
    minExp: 20,
    maxExp: 30,
    healing: 10,
  );
  Boss boss2 = Boss(
    name: 'Dark Lord',
    hp: 300,
    baseHp: 300,
    minDamage: 20,
    maxDamage: 25,
    minExp: 30,
    maxExp: 40,
    healing: 15,
  );
  Boss boss3 = Boss(
    name: 'The One',
    hp: 500,
    baseHp: 500,
    minDamage: 30,
    maxDamage: 40,
    minExp: 50,
    maxExp: 60,
    healing: 20,
  );

  final rnd = new Random();

  Player start(Player player) {
    print('=== DUNGEON ===');

    var level1Status = level(player, 1);
    if (level1Status != 'clear') return player;
    var level2Status = level(player, 2);
    if (level2Status != 'clear') return player;
    var level3Status = level(player, 3);
    if (level3Status != 'clear') return player;
    return player;
  }

  String level(Player player, int level) {
    print('=== LEVEL ${level} ===');
    player.setStamina = player.getStamina - 5;
    int state = 0;
    String status = '';

    while (state < 4) {
      var monster = null;
      var boss = null;
      player.printInfo();

      if (state == 3) {
        if (level == 1) boss = boss1;
        if (level == 2) boss = boss2;
        if (level == 3) boss = boss3;
        boss.resetHp();
        boss.printInfo();
      } else {
        if (level == 1) monster = monsterLv1[state];
        if (level == 2) monster = monsterLv2[state];
        if (level == 3) monster = monsterLv3[state];
        monster.resetHp();
        monster.printInfo();
      }

      print('1: Fight');
      print('2: Escape');
      print('Enter number: ');

      if (player.stamina < 5) {
        print('You don\'t have enough stamina to fight');
        break;
      }

      int? n = int.parse(stdin.readLineSync()!);
      if (n == 1) {
        if (state == 3) {
          fightBoss(player, boss);
          monster = boss;
        } else {
          fight(player, monster);
        }

        if (player.getHp > 0 && monster.getHp > 0) {
          print('You\'ve runaway from monster ${monster.getName}');
          status = 'runaway';
          break;
        } else if (player.getHp <= 0) {
          print('You\'ve died');
          player.hp = 100;
          status = 'dead';
          break;
        } else {
          print('You cleared state ${state + 1}/3');
          state++;
        }
      } else if (n == 2) {
        print('You\'ve escaped');
        status = 'escape';
        break;
      }
    }

    if (state == 3 &&
        status != 'dead' &&
        status != 'escape' &&
        status != 'runaway') {
      status = 'clear';
    }

    return status;
  }

  void fight(Player player, Monster monster) {
    while (monster.getHp > 0) {
      print('=== FIGHT MODE ===');
      player.printInfo();
      monster.printInfo();

      print('1: Attack');
      print('2: Leave');
      print('Enter number: ');

      int? n = int.parse(stdin.readLineSync()!);

      if (n == 1) {
        int playerDamage = player.getRandomDamage();
        monster.setHp = monster.getHp - playerDamage;
        print('You attacked ${monster.name} and gave ${playerDamage} damage');

        if (monster.getHp <= 0) {
          print('You killed ${monster.name}');
          int exp = monster.getRandomExp();
          print('You got ${exp} exp');
          player.setExp = player.getExp + exp;
          player.levelCalculator();
          break;
        } else {
          int monsterDamage = monster.getRandomDamage();
          player.setHp = player.getHp - monsterDamage;
          print(
              '${monster.name} attacked you and gave ${monsterDamage} damage');
          if (player.getHp <= 0) {
            break;
          }
        }
      } else {
        break;
      }
    }
  }

  void fightBoss(Player player, Boss boss) {
    while (boss.getHp > 0) {
      print('=== FIGHT MODE ===');
      player.printInfo();
      boss.printInfo();

      print('1: Attack');
      print('2: Leave');
      print('Enter number: ');

      int? n = int.parse(stdin.readLineSync()!);

      if (n == 1) {
        int playerDamage = player.getRandomDamage();
        boss.setHp = boss.getHp - playerDamage;
        print('You attacked ${boss.name} and gave ${playerDamage} damage');

        if (boss.getHp <= 0) {
          print('You killed ${boss.name}');
          int exp = boss.getRandomExp();
          print('You got ${exp} exp');
          player.setExp = player.getExp + exp;
          player.levelCalculator();
          break;
        } else {
          int bossDamage = boss.getRandomDamage();
          player.setHp = player.getHp - bossDamage;
          print('${boss.name} attacked you and gave ${bossDamage} damage');
          if (player.getHp <= 0) {
            break;
          } else {
            boss.setHp = boss.getHp + boss.healing;
            print('${boss.name} healed himself');
          }
        }
      } else {
        break;
      }
    }
  }
}
