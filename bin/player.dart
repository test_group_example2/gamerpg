import 'dart:io';
import 'dart:math';

class Player {
  int hp;
  int stamina;
  int baseDamage = 10;
  int exp = 0;
  int level = 1;
  int get getHp => this.hp;

  final rnd = new Random();

  Player({required this.hp, required this.stamina});

  set setHp(int hp) => this.hp = hp;

  get getStamina => this.stamina;

  set setStamina(stamina) => this.stamina = stamina;

  get getBaseDamage => this.baseDamage;

  set setBaseDamage(baseDamage) => this.baseDamage = baseDamage;

  get getExp => this.exp;

  set setExp(exp) => this.exp = exp;

  get getLevel => this.level;

  set setLevel(level) => this.level = level;

  int getRandomDamage() {
    return rnd.nextInt(this.baseDamage) + 1;
  }

  void levelCalculator() {
    if (this.exp >= 10) {
      this.level++;
      this.exp = 0;
      this.hp = 100 + (this.level * 10);
      this.stamina = 100 + (this.level * 10);
      this.baseDamage += 10;
      print('You leveled up to ${this.level}!');
    }
  }

  void printInfo() {
    print(
        '[PLAYER] HP: ${this.hp} Stamina: ${this.stamina} BaseDamage: ${this.baseDamage} Exp: ${this.exp} Level: ${this.level}');
  }
}
