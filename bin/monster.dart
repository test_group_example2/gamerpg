import 'dart:math';

class Monster {
  String name;
  int hp;
  int baseHp;
  int minDamage;
  int maxDamage;
  int minExp;
  int maxExp;

  Monster({
    required this.name,
    required this.hp,
    required this.baseHp,
    required this.minDamage,
    required this.maxDamage,
    required this.minExp,
    required this.maxExp,
  });

  String get getName => this.name;

  set setName(String name) => this.name = name;

  get getHp => this.hp;

  set setHp(hp) => this.hp = hp;

  get getMinDamage => this.minDamage;

  set setMinDamage(minDamage) => this.minDamage = minDamage;

  get getMaxDamage => this.maxDamage;

  set setMaxDamage(maxDamage) => this.maxDamage = maxDamage;

  get getMinExp => this.minExp;

  set setMinExp(minExp) => this.minExp = minExp;

  get getMaxExp => this.maxExp;

  set setMaxExp(maxExp) => this.maxExp = maxExp;

  void resetHp() {
    this.hp = this.baseHp;
  }

  int getRandomDamage() {
    return Random().nextInt(this.maxDamage) + this.minDamage;
  }

  int getRandomExp() {
    return Random().nextInt(this.maxExp) + this.minExp;
  }

  void printInfo() {
    print(
        '[MONSTER] Name: ${this.name} HP: ${this.hp} MinDamage: ${this.minDamage} MaxDamage: ${this.maxDamage} MinExp: ${this.minExp} MaxExp: ${this.maxExp}');
  }
}
