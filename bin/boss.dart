import 'monster.dart';

class Boss extends Monster {
  int healing;

  Boss({
    required super.name,
    required super.hp,
    required super.baseHp,
    required super.minDamage,
    required super.maxDamage,
    required super.minExp,
    required super.maxExp,
    required this.healing,
  });

  get getHealing => this.healing;
  set setHealing(int healing) => this.healing = healing;

  void heal() {
    print('$name is healing himself');
    hp += healing;
    if (hp > baseHp) {
      hp = baseHp;
    }
  }

  void printInfo() {
    print(
        '[BOSS] Name: ${this.name} HP: ${this.hp} MinDamage: ${this.minDamage} MaxDamage: ${this.maxDamage} MinExp: ${this.minExp} MaxExp: ${this.maxExp} Healing: ${this.healing}');
  }
}
