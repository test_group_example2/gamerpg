import 'player.dart';
import 'shop.dart';
import 'dungeon.dart';
import 'dart:io';

void main() {
  Shop shop = Shop();
  Dungeon dungeon = Dungeon();
  Player player = new Player(hp: 100, stamina: 100);

  while (true) {
    String selectedPortal = portal();
    if (selectedPortal == 'shop') {
      player = shop.open(player);
    } else if (selectedPortal == 'dungeon') {
      player = dungeon.start(player);
    } else if (selectedPortal == 'close') {
      break;
    }
  }
}

String portal() {
  print('=== PORTAL ===');
  print('1: ร้านค้า');
  print('2: เข้าสู่ดันเจี้ยน ชั่นที่ 1');
  print('3: กลับไปหน้าเริ่มต้น');
  print('Select portal: ');

  int? n = int.parse(stdin.readLineSync()!);

  if (n == 1) {
    return 'shop';
  }

  if (n == 2) {
    return 'dungeon';
  }

  if (n == 3) {
    print("Closing game");
    return 'close';
  }
  return 'error';
}
